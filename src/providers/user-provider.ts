import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Apollo , ApolloQueryObservable} from 'apollo-angular';
import gpl from 'graphql-tag';
import {Observable} from 'rxjs/Observable';
import 'rxjs';
import {GET_FEED_QUERY} from '../gpl/index'

/*
  Generated class for the User provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class UserProvider {

  constructor(public http: Http, public apollo: Apollo) {
    console.log('Hello User Provider');
  }

  public getFeed(){
  	var a = gpl`
  	query getFeed($user: UserInput){
	  getFeed(user:$user){
	    id
	    artist
	    title
	    albumArtUrl
	    date
	    listenon
	    links{
	      youtube
	      playmusic
	      spotify
	    }
	    author{
				username      
	    }
	  }
	}`;
	return this.apollo.watchQuery({
		query : a,
		variables : {
			user: {
				email : window.localStorage.getItem('email')
			}
		}
	});
	}

	public getUserProfile(email){
		let a =  gpl`
  	query user($user: UserInput){
	  user(user: $user){
	    first_name
	    last_name
	    email
	    username
	    lync{
	      title
	      artist
	      albumArtUrl
	      date
	      listenon
	      author{
	        username
	      }
	      links{
	        youtube
	        playmusic
	        spotify
	      }
	    }
	  }
	}
  	`
  	return this.apollo.watchQuery({
  		query: a,
  			variables: {
  				user: {
  					email: email
  				}
  			}
  		})
 	}

 	public follow(follower, to){
    var q = gpl`
      mutation follow($follower: String, $to: String){
      follow(follower: {email: $follower}, to:{email : $to})      
    }
    `;
    return this.apollo.mutate({
      mutation: q, 
      variables: {
        follower: follower,
        to: to
      }
    })
  }

  public unfollow(follower, to){
    var q = gpl`
      mutation unfollow($follower: String, $to: String){
      unfollow(follower: {email: $follower}, to:{email : $to})      
    }
    `;
    return this.apollo.mutate({
      mutation: q, 
      variables: {
        follower: follower,
        to: to
      }
    })
  }


}
