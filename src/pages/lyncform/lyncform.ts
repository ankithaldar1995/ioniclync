import { Component, Input , ChangeDetectionStrategy} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the Lyncform page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-lyncform',
  templateUrl: 'lyncform.html',
  changeDetection : ChangeDetectionStrategy.OnPush
})
export class Lyncform {
	@Input() data: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Lyncform');
  }

}
