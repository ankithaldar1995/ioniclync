import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Lyncform } from './lyncform';

@NgModule({
  declarations: [
    Lyncform,
  ],
  imports: [
    IonicPageModule.forChild(Lyncform),
  ],
  exports: [
    Lyncform
  ]
})
export class LyncformModule {}
