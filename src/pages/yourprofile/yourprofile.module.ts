import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Yourprofile } from './yourprofile';

@NgModule({
  declarations: [
    Yourprofile,
  ],
  imports: [
    IonicPageModule.forChild(Yourprofile),
  ],
  exports: [
    Yourprofile
  ]
})
export class YourprofileModule {}
