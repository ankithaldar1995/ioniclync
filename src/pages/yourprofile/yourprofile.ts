import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Store} from '@ngrx/store';
import  {AppState, SELECT_LYNC} from '../../reducers/current-playing.reducer';
import { UserProvider} from '../../providers/index'
import {Postdetail} from '../index'


/**
 * Generated class for the Yourprofile page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-yourprofile',
  templateUrl: 'yourprofile.html',
})
export class Yourprofile {
public data : any;
public isFollowing : boolean = false;
  constructor(public navCtrl: NavController, 
    public navParams: NavParams, public store : Store<AppState>,
  public userService : UserProvider) {
  	this.store.select('currentPlayingReducer').map((x:any)=>x.selectedUser).subscribe(x=>{
      console.log(x)
    	if(x!=null){
    		this.data = x 
        this.isFollowing = x.isFollowing;
    	}
    })
    this.userService.getUserProfile(this.data.email).subscribe((x:any)=>{
      console.log(x)
      this.data = x.data.user[0]
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Yourprofile');
  }

  follow(){
    this.userService.follow(window.localStorage.getItem('email'),this.data.email ).subscribe(x=>{
      console.log(x, "Asdsa")
    });
  }

  unfollow(){
    this.userService.unfollow(window.localStorage.getItem('email'),this.data.email ).subscribe(x=>{
      console.log(x, "Asdsa")
    });
  }

  info(item){
    this.store.dispatch({type: SELECT_LYNC, payload: {lync : item}})
    this.navCtrl.push(Postdetail)
  }



}
