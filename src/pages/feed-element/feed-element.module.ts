import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeedElement } from './feed-element';

@NgModule({
  declarations: [
    FeedElement,
  ],
  imports: [
    IonicPageModule.forChild(FeedElement),
  ],
  exports: [
    FeedElement
  ]
})
export class FeedElementModule {}
