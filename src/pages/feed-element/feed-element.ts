import { Component, Input } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the FeedElement page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-feed-element',
  templateUrl: 'feed-element.html',
})
export class FeedElement {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
@Input('username') un  : any;
@Input('item') item : any;

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeedElement');
  }

}
