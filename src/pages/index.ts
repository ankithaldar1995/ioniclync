export { Feed } from './feed/feed';
export {Signup} from './signup/signup';
export  {Profile} from './profile/profile';
export { Search } from './search/search';
export { Postdetail } from './postdetail/postdetail';
export { Lyncmusic } from './lyncmusic/lyncmusic';
export { TabsPage } from './tabs/tabs';
export  {Yourprofile} from './yourprofile/yourprofile';