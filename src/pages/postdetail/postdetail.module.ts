import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Postdetail } from './postdetail';

@NgModule({
  declarations: [
    Postdetail,
  ],
  imports: [
    IonicPageModule.forChild(Postdetail),
  ],
  exports: [
    Postdetail
  ]
})
export class PostdetailModule {}
