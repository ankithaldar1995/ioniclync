import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Store} from '@ngrx/store';
import  {AppState, SELECT_LYNC} from '../../reducers/current-playing.reducer';
/**
 * Generated class for the Postdetail page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-postdetail',
  templateUrl: 'postdetail.html',
})
export class Postdetail {
public item2 = {
			id: 1,
			artist : "Guns n' Roses",
			title : "Sweet Child O' Mine",
			album: 'Album One',
			imgUrl: "assets/img/gKJFiDd6Qk6CMxmy0cRs_sweet_child_of_mine.jpg",
			genre : "Rock",
			comments : [{
				avatar: "assets/img/wRt3cUQqThqx9ybX7pAb_cartoon_avatar.jpg",
				by: '@steven',
				text : "Awesome!"
			},
			{
				avatar: "assets/img/wRt3cUQqThqx9ybX7pAb_cartoon_avatar.jpg",
				by: '@craig',
				text : "Too good man!"
			}],
			likes : 20,
			relync: 12,
			author_name : "Marty Mc'Fly",
			author_hashname : "@martyFly",
			date: "November 5, 2025",
			listenon : "soundcloud",
			caption: "Coooool #superb",
			when : "2 hours",
			links :{
				youtube: "",
				spotify: "",
				soundcloud: ""
			}
		};
public item;

  constructor(public navCtrl: NavController, public navParams: NavParams, public store : Store<AppState>) {
  	this.store.select('currentPlayingReducer').map((x:any)=>x.currentLync).subscribe(x=>{
  		console.log(x);
  		this.item = x.lync;
  	})
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Postdetail');
  }

}
