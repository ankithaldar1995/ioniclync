import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Signup, TabsPage} from '../index';
import { Http, RequestOptions, Headers } from '@angular/http';
import { ToastController } from 'ionic-angular';
import {LoginService} from '../../services/index';
import { LoadingController } from 'ionic-angular';

/**
 * Generated class for the Welcomeslider page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */



export class MyPage {
  constructor() {
  }

  
}
@IonicPage()
@Component({
  selector: 'page-welcomeslider',
  templateUrl: 'welcomeslider.html',
})
export class Welcomeslider {
	signin;
	signup;
  email;
  password;
  loginProcess = false;
  loginUrl = 'http://lyncmusicapp.herokuapp.com/api/v1/users/sign_in';
  loader = false;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams, 
    public http : Http, 
    public toastCtrl: ToastController, 
    public loginService : LoginService, 
    public loadingCtrl: LoadingController) {
    	this.signin = TabsPage;
    	this.signup = Signup;
      if(window.localStorage.getItem('auth')){
        this.navCtrl.push(TabsPage);
      }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Welcomeslider');
  }
  login(){
    this.loader = true;
    this.http.post(this.loginUrl, {email:this.email,  password: this.password})
      .subscribe((x : any)=>{
        this.loader = false;
        x=x.json();
        console.log("res",x);
        this.loginService.authenticate(x);
        if(x.authentication_token){
          console.log("Jaipur");
          this.navCtrl.push(TabsPage);
        }
      }, err=>{
        this.loader = false;
        err = err.json()
        console.log("err",err);

        if(err.errors == "Invalid email or password"){
           console.log("ahhaha")
          this.presentToast("Invalid email or password. Please check & try again");
        }
        else {
          this.presentToast("ERROR - Cannot connect");
        }
      })
    }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }
  


}
