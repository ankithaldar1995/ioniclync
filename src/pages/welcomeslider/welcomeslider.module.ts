import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Welcomeslider } from './welcomeslider';

@NgModule({
  declarations: [
    Welcomeslider,
  ],
  imports: [
    IonicPageModule.forChild(Welcomeslider),
  ],
  exports: [
    Welcomeslider
  ]
})
export class WelcomesliderModule {}
