import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { Http } from '@angular/http';
/**
 * Generated class for the Signup page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */


@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class Signup {

  constructor(public navCtrl: NavController, public navParams: NavParams, public http : Http,public toastCtrl: ToastController, ) {
  }
  username;
  first_name;
  last_name;
  password;
  email;
  SignupUrl = 'https://lyncmusicapp.herokuapp.com/api/v1/users/sign_up'

  ionViewDidLoad() {
    console.log('ionViewDidLoad Signup');
  }

  register(){
  	let user = {
  		username : this.username,
  		first_name : this.first_name,
  		last_name : this.last_name,
  		password : this.password,
  		email : this.email
  	}
  	console.log(user);
  	this.http.post(this.SignupUrl, user ).subscribe(x=>{
  		console.log("res",x);
      this.presentToast("Please verify by clicking on the link sent on your email");
  	})
  }
   presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }

}
