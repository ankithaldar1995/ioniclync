import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';

import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Feed } from '../feed/feed';
import { Profile } from '../profile/profile';
import { Search } from '../search/search';

/**
 * Generated class for the Tabs page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
	feed;
	search;
	profile;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	this.feed = Feed;
  	this.profile = Profile;
  	this.search = Search;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Tabs');
  }

}
