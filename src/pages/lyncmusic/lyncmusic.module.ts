import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Lyncmusic } from './lyncmusic';

@NgModule({
  declarations: [
    Lyncmusic,
  ],
  imports: [
    IonicPageModule.forChild(Lyncmusic),
  ],
  exports: [
    Lyncmusic
  ]
})
export class LyncmusicModule {}
