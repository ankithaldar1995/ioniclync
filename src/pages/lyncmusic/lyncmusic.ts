import { Component, ChangeDetectionStrategy, ChangeDetectorRef, Input} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Store} from '@ngrx/store';
import  {AppState, INCREASE} from '../../reducers/current-playing.reducer';
import { Apollo , ApolloQueryObservable} from 'apollo-angular';
import gpl from 'graphql-tag';
import {Observable} from 'rxjs/Observable';
import 'rxjs';
import {ToastService} from '../../services/index';

/**
 * Generated class for the Lyncmusic page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-lyncmusic',
  templateUrl: 'lyncmusic.html',
  changeDetection : ChangeDetectionStrategy.OnPush
})
export class Lyncmusic {
	public data$;
  public item;
  public on;
  public enableLync = false
  public lync = {
    id: 12,
    artist : "Guns n' Roses",
    title : "Sweet Child O' Mine",
    album: 'Album One',
    albumArtUrl: "assets/img/gKJFiDd6Qk6CMxmy0cRs_sweet_child_of_mine.jpg",
    image: "https://songlink.io/image?url=http://is3.mzstatic.com/image/thumb/Music22/v4/56/1d/9b/561d9b79-1927-f13b-9886-5cd3d921ab13/source/512x512bb.jpg",
    genre : "Rock",
    author: {
      id : 1, 
      first_name : "Marty",
      last_name : "Mc'Fly",
      username : "@martyFly",
    },
    date: "November 5, 2025",
    listenon : "soundcloud",
    caption: "Coooool #superb",
    when : "2 hours",
    links :{
        applemusic : "https://itunes.apple.com/album/magma/id1104432592?i=1104432811&uo=4&ls=1",
        youtube : "https://www.youtube.com/watch?v=dLC9CHxNHHU",
        playmusic : "https://play.google.com/music/m/Tcyqdrrvxq6jskvqpjqnoqx2pu4?signup_if_needed=1",
        deezer : "https://www.deezer.com/track/126338371",
        twtshare : "https://twitter.com/intent/tweet?text=https://songlink.io/i/1104432811",
        fbshare : "https://www.facebook.com/sharer/sharer.php?u=https://songlink.io/i/1104432811",
      }
  }

   constructor(
   public navCtrl: NavController,
   public navParams: NavParams,
   public store : Store<AppState>,
   private apollo: Apollo, 
   public taost : ToastService, public cdr : ChangeDetectorRef) {

    setInterval(() => {
      this.cdr.markForCheck();
    }, 1000);

    this.store.select('currentPlayingReducer').map((x: any)=>x.nowPlaying).subscribe(x=>{
      this.item = x;
    })
    this.store.select('currentPlayingReducer').map((x: any)=>x.on).subscribe(x=>{
      this.on = x
    })

    // this.items = this.store.select('currentPlayingReducer').map((x: any)=>x.i)
  }
  public storeUp(){
    this.store.dispatch({
      type : INCREASE
    })
  }

  test(){
    const query = gpl`
    {
      user{
          email
        }
      }
    `;
    this.apollo.watchQuery({
      query: query
    }).subscribe(res=>{
      console.log("uayyat", res);
    })
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad Lyncmusic');
  }

  lyncNow(item){
    this.enableLync = true
    this.taost.presentToast("Lync Posted!")
    console.log("lyncing", item);
    const a = gpl`
    mutation addLync($input : LyncInput!){
        addLync(input : $input){
          title
          artist
        }
      }`;
      this.apollo.mutate({
      mutation : a,
      variables : {
         input : {
             id: item.id,
            artist :  item.artist,
            title :  item.title,
            albumArtUrl:  item.image,
            author: {
              email: window.localStorage.getItem('email'),
            },
            date: Date.now(),
            listenon : this.on,
            links :{
                applemusic : item.applemusic,
                youtube : item.youtube,
                playmusic : item.playmusic,
                deezer : item.deezer,
                spotify : item.spotify
              }
          }
      },
      optimisticResponse: {
        __typename: 'Mutation',
          addLync: {
            __typename: "Lync",
            title: item.title,
            artist : item.artist
          },
      }
    }).subscribe(res=>{
      this.enableLync = false
      console.log("resaaa", res);
      if((<any>res).data.addLync){{
        this.taost.presentToast("Lync Posted!")
      }}
    });
  }
}
