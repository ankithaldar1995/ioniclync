import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Apollo , ApolloQueryObservable} from 'apollo-angular';
import 'rxjs';
import {Observable} from 'rxjs/Observable';
import gpl from 'graphql-tag';
import {Store} from '@ngrx/store';
import  {AppState, SEARCHED_USER} from '../../reducers/current-playing.reducer';
import {Profile} from '../profile/profile'
import {Yourprofile} from '../yourprofile/yourprofile'
/**
 * Generated class for the Search page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class Search {
public searchText;
public item : Observable<any>;
  constructor(public navCtrl: NavController, public navParams: NavParams, public apollo: Apollo, public store : Store<AppState>) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Search');
  }

  search(){
  	var q = gpl`
  	query search($user:UserInput, $text : String){
  		search(user:$user, text:$text){
		    first_name
		    email
        username
        last_name
        username
        isFollowing
		  }	
  	}`;
  	this.item = this.apollo.query({
  		query: q,
  		variables: {
  			text :this.searchText,
        user : {
          email :  window.localStorage.getItem('email')
        }
  		}
  	}).debounceTime(100).map((x : any)=>x.data.search);
  }

  openProfile(item){
    this.store.dispatch({type: SEARCHED_USER, payload: item})
    this.navCtrl.push(Yourprofile)
  }


  follow(data){
  	var q = gpl`
  		mutation follow($follower: String, $to: String){
		  follow(follower: {email: $follower}, to:{email : $to})		  
		}
  	`;
  	console.log(data)
  	this.apollo.mutate({
  		mutation: q, 
  		variables: {
  			follower: window.localStorage.getItem('email'),
  			to: data
  		}
  	}).subscribe();
  }

}
