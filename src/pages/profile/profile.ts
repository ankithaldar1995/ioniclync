import { Component, ChangeDetectionStrategy} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Store} from '@ngrx/store';
import  {AppState, SELECT_LYNC} from '../../reducers/current-playing.reducer';
import { Apollo , ApolloQueryObservable} from 'apollo-angular';
import gpl from 'graphql-tag';
import {Observable} from 'rxjs/Observable';
import 'rxjs';
import {ToastService} from '../../services/index';
import { Postdetail } from '../postdetail/postdetail';
import { Welcomeslider } from '../welcomeslider/welcomeslider';
import { UserProvider} from '../../providers/index'


/**
 * Generated class for the Profile page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class Profile {
public data : any;
public email : string;
public isFollowing : boolean = false;
public searchedUser : boolean;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
     public apollo : Apollo, public store : Store<AppState>, public userService : UserProvider) {
    console.log("ptrog")
    this.email = window.localStorage.getItem('email')
    this.getUserProfile()
  }

  ionViewWillLeave(){
    
  }

    ionViewWillEnter(){
      console.log("jsjsjs")
    }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad Profile');
    // this.store.select('currentPlayingReducer').map((x:any)=>x.selectedUser).subscribe(x=>{
    // 	if(x!=null){
    // 		this.data = x
    //     this.email = x.email
    //     if(x.isFollowing == true){
    //       this.isFollowing = x.isFollowing
    //     }
    //     (x.email == window.localStorage.getItem('email'))?this.searchedUser = false:this.searchedUser = true;
    //     this.getUserProfile()
    // 	}
    // 	else {
    //     this.email = window.localStorage.getItem('email')
    // 		this.getUserProfile()
    //     this.searchedUser = false
    // 	}
    // })
  }

  info(item){
  	this.store.dispatch({type: SELECT_LYNC, payload: {lync : item}})
  	this.navCtrl.push(Postdetail)
  }

  logOut(){
  	var a = gpl`
  	mutation logout($email: String){
  		logout(email : $email)
  	}
  	`;
  	this.apollo.mutate({
  		mutation: a, 
  		variables: window.localStorage.getItem('email')
  	}).subscribe(res=>{
  		console.log(res, "loggedout")
  		window.localStorage.clear()
  		this.navCtrl.push(Welcomeslider);
  	})
  }

  getUserProfile(){
    this.userService.getUserProfile(this.email).subscribe((res : any)=>{
      console.log(res)
      this.data = res.data.user[0];
    })
  }
  
}
