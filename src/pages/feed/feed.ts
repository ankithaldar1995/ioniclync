import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Postdetail } from '../postdetail/postdetail';
import { Lyncmusic } from '../lyncmusic/lyncmusic';
import { Apollo , ApolloQueryObservable} from 'apollo-angular';
import gpl from 'graphql-tag';
import {Observable} from 'rxjs/Observable';
import 'rxjs';
import {ToastService} from '../../services/index';
import {Store} from '@ngrx/store';
import  {AppState, SELECT_LYNC} from '../../reducers/current-playing.reducer';
import { UserProvider } from '../../providers/index'
/**
 * Generated class for the Feed page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-feed',
  templateUrl: 'feed.html',
})
export class Feed {
	public data : ApolloQueryObservable<any> ;
	public feedItem : any = [];
	
  constructor(
  	public navCtrl: NavController,
  	public navParams: NavParams, 
   	public apollo : Apollo, 
 	public store : Store<AppState>,
   	public userProvider : UserProvider) {
  	this.getFeed();
  	// this.buildFeed();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Feed');
  }


  public lyncMusic(){
  	this.navCtrl.push(Lyncmusic);
  }

  public info(item){
    this.store.dispatch({type: SELECT_LYNC, payload: {lync : item}})
    this.navCtrl.push(Postdetail)
  }


  public getFeed(){
 	this.data = this.userProvider.getFeed()
  }

  public refresh(){
  	this.data.refetch()
  }

  

}
