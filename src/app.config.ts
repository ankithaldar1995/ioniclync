export class Config{
	public graphUrl  : string = 'http://localhost:8000/graphql';
	// public graphUrl = 'https://shielded-gorge-61336.herokuapp.com/graphql';

	// public registerLoginUrl : string  = "http://localhost:8000/registerLogin"
	public registerLoginUrl = 'https://shielded-gorge-61336.herokuapp.com/registerLogin';
}