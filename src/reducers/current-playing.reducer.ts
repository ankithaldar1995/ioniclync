import { Blog } from '../models/app.model';
import { ActionReducer, Action } from '@ngrx/store';
import * as _ from 'underscore';

export const PLAYING_NOW = 'PLAYING_NOW';
export const PLAYING_NOW_ON = 'PLAYING_NOW_ON';
export const SELECT_LYNC = 'SELECT_LYNC';
export const SEARCHED_USER = "SEARCHED_USER";
export const REMOVE_SEARCHED_USER = "REMOVE_SEARCHED_USER";
export const INCREASE = "INCREASE";
export const ADD_MUSIC = 'ADD_MUSIC';

export interface AppState {
    nowPlaying : any ,
    on: string,
    currentLync : any
    selectedUser : any
    i : any
    music : any
    // blogs : Blog[]
    // selectedBlog : string
    // selectedGenre : string
    // loading : boolean
}

export const initialState : AppState = {
    nowPlaying : [],
    on: null,
    currentLync : null,
    selectedUser : null,
    i : 0,
    music : []
    // selectedBlog : null,
    // selectedGenre : 'allNews',
    // loading : false
};

// var a;
// const details =  (state, action) =>{
//     switch (action.type) {
//         case SAVE_COMMENT:
//         if(state.title === action.payload.title){
//             console.log(state);
//             return Object.assign({}, state , {comment : state.comment.concat(action.payload.comment)});
//         }
//         return state;

//         default:
//             return state;
//     }
// }

export const currentPlayingReducer = (state = initialState, action :Action) => {
    switch (action.type) {
        case PLAYING_NOW:
            return Object.assign({}, state, {
                nowPlaying : action.payload
            });
        case PLAYING_NOW_ON : 
            return Object.assign({}, state, {
                on: [action.payload]
            });
        case SELECT_LYNC : 
            return Object.assign({}, state, {
                currentLync : action.payload
            })
        case SEARCHED_USER : 
            return Object.assign({}, state, {
                selectedUser : action.payload
            })
        case REMOVE_SEARCHED_USER :
            return Object.assign({}, state, {
                selectedUser : null
            })
         case INCREASE :{
             return Object.assign({}, state, {
                 i : Date.now()
             })
         }
        case ADD_MUSIC : {
            return Object.assign({}, state, {
                music : [action.payload, ...state.music]
            } )
        }
        // case LOAD_BLOGS_SUCCESS:
        //     return Object.assign({}, state, {loading : false});

        // case LOAD_BLOGS_FAILED:
        //     console.log("Cannot load the blogs");
        //     return Object.assign({}, state, {loading : false});

        // case SAVE_COMMENT:
        //     const {title, comment} = action.payload;
        //     return Object.assign( {} , { blogs: state.blogs.map(states => details(states, action)) } );

        // case SELECT_GENRE :
        //     return Object.assign({}, state, {selectedGenre : action.payload.genre});

        // case ADD_BLOG:
        //     return Object.assign({}, state, {blogs : [action.payload, ...state.blogs]});

        // case SELECT_BLOG:
        //     return Object.assign({}, state, {selectedBlog: action.payload});
                   
        default:
            return state;
    }
}