import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs';


@Injectable()
export class LinkService{
	constructor(public http : Http){
	}

	getLinks(title, artist): Observable<any>{
		var url = "https://dry-journey-80543.herokuapp.com/link?artist=" + artist + "&title=" + title;

		return this.http.get(url);
	}
}