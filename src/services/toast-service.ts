import {Injectable} from '@angular/core';
import { ToastController} from 'ionic-angular';

@Injectable()
export class ToastService{
	
	constructor(public toast : ToastController){
	}

	presentToast(msg) {
    let toast = this.toast.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }
}