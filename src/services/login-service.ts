import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import { NavController} from 'ionic-angular';
import {Signin} from '../pages/signin/signin';
import { Http } from '@angular/http';
import { Config } from '../app.config';
@Injectable()
export class LoginService{
	public config : any = new Config();
	public token: string;
	public email: string;

	constructor(public http : Http){
	}

	authenticate(data){
		console.log(data,"agga" );
		this.email = data.email;
		this.token = data.authentication_token;
		window.localStorage.setItem('auth', this.token);
		window.localStorage.setItem('email', this.email);
		this.registerLogin(data);
	}
	// alreadyLoggedIn(){
	// 	this.navCtrl.push(Signin);
	// }
	registerLogin(data){
        // let data = {token: this.token, email: this.email};
        this.http.post(this.config.registerLoginUrl, data).subscribe();
	}
}