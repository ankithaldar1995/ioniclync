import {Injectable} from '@angular/core';
import {LinkService} from './link-service';
import {Observable} from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import {
	AppState,PLAYING_NOW , PLAYING_NOW_ON, ADD_MUSIC
} from '../reducers/current-playing.reducer';
import 'rxjs/add/operator/map';



@Injectable()
export class SongFilterService{
	constructor(public linkService : LinkService, public store : Store<AppState>){}
currentSong;
searching = false;
	public filterWithPackageName(data){
		console.log(data)
		// switch (data.package) {
		// 	case "com.google.android.music":
		// 		// code...
		// 		this.playMusic(data);
		// 		break;
			
		// 	default:
		// 		// code...
		// 		break;
		// }

		if(data.package == "com.google.android.music" || data.package == "com.spotify.music"){
			if(this.currentSong == data) return ;
			if(this.searching && !(this.currentSong == undefined)) return ;
			console.log(data);
			return this.playMusic(data);
		}
	}

	public playMusic(data){
		this.store.dispatch({type: PLAYING_NOW_ON, payload:'playmusic' })
		console.log("data : ", data);
		let songTitle = data.title;
		let songArtist = data.text;
		this.searching = true;
		this.linkService.getLinks(songTitle, songArtist).map(x=>{
			x=x.json();
			console.log(x, "from service")
			if(x.status == false) {
				//show pop that music not found, wanna edit what you have
				return ;
			}
			else{
				return x
			}
		}).subscribe(x=>{
			// if(x==undefined){
			// 	x=null;
			// }
			console.log(x);
			this.store.dispatch({
				type: PLAYING_NOW,
				payload: x
			})
			this.store.dispatch({
				type : ADD_MUSIC, 
				payload : x
			})
			this.currentSong =  data;
			this.searching = false;
		})
		//dispatch acrion for change song
	}
}