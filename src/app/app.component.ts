import { Component } from '@angular/core';
import { Platform, NavController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/index';
import {SongFilterService} from '../services/song-filter-service';
import { Welcomeslider } from '../pages/welcomeslider/welcomeslider';
// import { Signin} from '../pages/signin/signin';
import {LoginService} from '../services/index';


declare var cordova: any;

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any 

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public filterService : SongFilterService, public loginService: LoginService) {
    this.routing();
    platform.ready().then(() => {
      
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      this.startingPlugin();

     
    });
  }

  routing(){
     if (window.localStorage.getItem('auth')) {
         this.rootPage = TabsPage
     } else {
         this.rootPage = Welcomeslider
     }
  }

  startingPlugin(){
    // var cordova:any;
    console.log("cordova", cordova)
    // console.log("test", cordova.callbacks.CoreAndroid551058213.success());
    var $this = this;
    cordova.define.moduleMap["net.coconauts.notification-listener.NotificationListener"].exports.listen(n=>{
    // this.items.push(n);
      // console.log("Received notification " + JSON.stringify(n) );
      // var message = n.title + '\n' ;
      // if (n.textLines) message += n.textLines;
      // else message += n.text;

      
      $this.filterService.filterWithPackageName(n);
      // bt.send("!nt:"+message );

    }, function(e){
      // setTimeout(setNotificationListenerCallback, 1000);
      console.log("Notification Error " + e);
    })
  }
}
