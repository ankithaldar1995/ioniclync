import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler, NavController } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { Welcomeslider } from '../pages/welcomeslider/welcomeslider';
import { Signup, Feed, Profile, Search, Postdetail, Lyncmusic} from '../pages/index';
import  { Yourprofile} from '../pages/yourprofile/yourprofile'


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpModule } from '@angular/http';

import { StoreModule } from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { StoreLogMonitorModule, useLogMonitor } from '@ngrx/store-log-monitor';
import {currentPlayingReducer} from '../reducers/current-playing.reducer';

import { ApolloClient ,createNetworkInterface} from 'apollo-client';
import { ApolloModule } from 'apollo-angular';

import {LinkService,SongFilterService, LoginService, ToastService} from '../services/index'
import { UserProvider} from '../providers/user-provider'

// var uri =  'http://localhost:8000/graphql'
var uri = 'https://shielded-gorge-61336.herokuapp.com/graphql'

const networkInterface = createNetworkInterface(uri);
networkInterface.use([{
  applyMiddleware(req, next) {
    if (!req.options.headers) {
      req.options.headers = {};  // Create the header object if needed.
    }
    // get the authentication token from local storage if it exists
    req.options.headers.Authorization = localStorage.getItem('auth') || null;
    req.options.headers.email = localStorage.getItem('email') || null;
    next();
  }
}]);

export function instrumentOptions() {
  return {
    monitor: useLogMonitor({ visible: true, position: 'right' })
  };
}
const client = new ApolloClient({
  networkInterface,
  // networkInterface : createNetworkInterface({
  //   uri: 'http://localhost:8000/graphql'
  //   // uri : 'https://shielded-gorge-61336.herokuapp.com/graphql'
  // }),
});

export function provideClient(): ApolloClient{
  return client;
}

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    Welcomeslider,
    Signup,
    Feed,
    Profile, 
    Search,
    Postdetail,
    Lyncmusic,
    Yourprofile
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    ApolloModule.forRoot(provideClient),
    HttpModule,
    StoreModule.provideStore({ currentPlayingReducer : currentPlayingReducer}),
    // EffectsModule.run(BlogEffectService)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    Welcomeslider,
    Signup,
    Feed,
    Profile, 
    Search,
    Postdetail,
    Lyncmusic,
    Yourprofile
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    LinkService,
    SongFilterService,
    LoginService,
    ToastService,
    UserProvider
  ]
})
export class AppModule {}
