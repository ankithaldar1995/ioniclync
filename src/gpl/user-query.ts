import gpl from 'graphql-tag';

export const GET_FEED_QUERY = gpl`
  	query getFeed($user: UserInput){
	  getFeed(user:$user){
	    id
	    artist
	    title
	    albumArtUrl
	    date
	    listenon
	    links{
	      youtube
	      playmusic
	      spotify
	    }
	    author{
				username      
	    }
	  }
	}`;

export const GET_USER_PROFILE = gpl`
  	query user($user: UserInput){
	  user(user: $user){
	    first_name
	    last_name
	    email
	    username
	    lync{
	      title
	      artist
	      albumArtUrl
	      date
	      listenon
	      author{
	        username
	      }
	      links{
	        youtube
	        playmusic
	        spotify
	      }
	    }
	  }
	}
  	`